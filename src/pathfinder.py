#!/usr/bin/env python3

import psycopg2
from config import host, dbname, username, password, port
from typing import List, Dict, Set

FIND_TABLE_FOREIGN_KEYS_QUERY = '''
    select 
    conrelid::regclass foreign_table_name, 
    a1.attname foreign_column_name, 
    confrelid::regclass table_name, 
    a2.attname column_name
    from (
    select conname, conrelid::regclass, confrelid::regclass, col, fcol
    from pg_constraint c, 
    lateral unnest(conkey) col, 
    lateral unnest(confkey) fcol
    where contype = 'f'
    ) s
    join pg_attribute a1 on a1.attrelid = conrelid and a1.attnum = col
    join pg_attribute a2 on a2.attrelid = confrelid and a2.attnum = fcol
    order by foreign_table_name;
'''


class Connection():
    def __init__(
        self,
        origin_table,
        origin_key,
        foreign_table,
        foreign_key,
    ) -> None:
        self.foreign_table = foreign_table
        self.foreign_key = foreign_key
        self.origin_table = origin_table
        self.origin_key = origin_key

    def __repr__(self) -> str:
        return f"{self.origin_table}->{self.foreign_table}"


class Node():
    def __init__(self, name='') -> None:
        self.name = name
        self.outgoing_connections: List[Connection] = []

    def add_outgoing_connection(self, connection: Connection):
        self.outgoing_connections.append(connection)

    def get_connected_table_names(self):
        return set(
            [conn.foreign_table.name for conn in self.outgoing_connections])

    def get_outgoing_connections(self) -> List[Connection]:
        return self.outgoing_connections

    def __str__(self) -> str:
        return f"{self.name}"

    def __repr__(self) -> str:
        return f"{self.name}"


class DFS():
    def __init__(self, adj_list, depth_limit=4) -> None:
        self.depth_limit = depth_limit
        self.adj_list = adj_list
        self.visited = {node: False for node in adj_list.keys()}
        self.current_path = []
        self.simple_paths = []

    def run(self, u, v):
        if self.visited[u]:
            return
        if len(self.current_path) >= self.depth_limit:
            return

        self.visited[u] = True
        self.current_path.append(u)

        if u == v:
            self.simple_paths.append(self.current_path[::])
            self.visited[u] = False
            self.current_path.pop()
            return

        for child in self.adj_list.get(u, set()):
            self.run(child, v)
        self.current_path.pop()
        self.visited[u] = False


class PathFinder():
    def __init__(self) -> None:
        self.relations = None
        self.adj_list = dict()
        self.conn = psycopg2.connect(host=host,
                                     port=port,
                                     dbname=dbname,
                                     user=username,
                                     password=password)

    def _build_relations(self):
        cursor = self.conn.cursor()
        cursor.execute(FIND_TABLE_FOREIGN_KEYS_QUERY)

        db_relations: Dict[str, Node] = dict()

        tables_relations = cursor.fetchall()
        for relation in tables_relations:
            foreign_table, foreign_key, origin_table, origin_key = relation
            if origin_table not in db_relations:
                db_relations[origin_table] = Node(name=origin_table)
            if foreign_table not in db_relations:
                db_relations[foreign_table] = Node(name=foreign_table)

            connection = Connection(
                db_relations[origin_table],
                origin_key,
                db_relations[foreign_table],
                foreign_key,
            )
            db_relations[origin_table].add_outgoing_connection(connection)

        self.relations = db_relations

    def _build_adjacency_list(self):
        for name, node in self.relations.items():
            connected_tables = node.get_connected_table_names()
            if name not in self.adj_list:
                self.adj_list[name] = connected_tables
            else:
                self.adj_list[name] = self.adj_list[name].union(
                    set(connected_tables))

            for table in connected_tables:
                if table not in self.adj_list:
                    self.adj_list[table] = set([name])
                else:
                    self.adj_list[table] = self.adj_list[table].union(
                        set([name]))

    def _get_connections_by_table_names(self, table_origin, table_foreign):
        connections = self.relations[table_origin].outgoing_connections
        connections.extend(self.relations[table_foreign].outgoing_connections)
        return [
            conn for conn in connections
            if (table_origin == conn.origin_table.name
                and table_foreign == conn.foreign_table.name) or (
                    table_origin == conn.foreign_table.name
                    and table_foreign == conn.origin_table.name)
        ]

    def find_all_paths_between_x_and_y(self, x, y):
        self._build_relations()
        self._build_adjacency_list()
        dfs = DFS(self.adj_list)
        dfs.run(x, y)
        paths = dfs.simple_paths
        paths = sorted(paths, key=lambda x: len(x), reverse=False)
        path_details = []
        for possible_path in (paths):
            connections = []
            for index, _ in enumerate(possible_path):
                if index <= len(possible_path) - 2:
                     connections.append(self._get_connections_by_table_names(
                         possible_path[index],
                         possible_path[index + 1]))
            path_details.append((possible_path, connections))

        return path_details


def main():
    pathfinder = PathFinder()
    print(pathfinder.find_all_paths_between_x_and_y("member", "invoice"))


if __name__ == "__main__":
    main()
